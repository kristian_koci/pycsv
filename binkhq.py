import csv
from operator import itemgetter
from collections import Counter
from datetime import datetime


def read_csv(data):
    """This function is for testing purposes"""
    with open(data, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

def current_rent(filename, numrows=5):
    rows = []
    try:
        with open(filename) as csvfile:
            reader = csv.DictReader(csvfile)
            for i, row in enumerate(reader, 1):
                rows.append(row)
                if i >= numrows:
                    break
    except FileNotFoundError:
        print("file {!r} does not exist".format(filename))
        return

    rows.sort(key=lambda row: float(itemgetter('Current Rent')(row)))
    for row in rows:
        print(', '.join(row.values()))

current_rent('MobilePhoneMasts.csv')

def filter_sum(filename):
    try:
        with open(filename, 'r', newline='') as f:
            c = csv.DictReader(f)
            filtering = [row for row in c if row['Lease Years'] == '25']

            f.seek(0)
            next(f)

            total = sum(float(row["Current Rent"]) for row in filtering)
            print(filtering, "The total is %s" % total)
    except FileNotFoundError:
        print("file {!r} does not exist".format(filename))
        return

filter_sum('MobilePhoneMasts.csv')

def dict_counter(filename):
    try:
        with open(filename, 'r', newline='') as f:
            r = csv.reader(f)
            cn = Counter(map(itemgetter(6), r))
            # print item that appears once, or more than once in the column
            for k, v in cn.items():
                if v >= 1:
                    print(k,v)
    except FileNotFoundError:
        print("file {!r} does not exist".format(filename))
        return

dict_counter('MobilePhoneMasts.csv')

def convert_csv_date():

    with open('MobilePhoneMasts.csv', 'r') as infile:
        # read the file as a dictionary for each row ({header : value})
        # it's hardcoded a bit, since I didn't have enough time to refactor it
        reader = csv.DictReader(infile)
        data = {}
        for row in reader:
            lines = row['Lease Start Date']
            reader = row['Tenant Name'], row['Property Address [1]'], row['Property  Address [2]'], \
            row['Property Address [3]'], row['Property Address [4]'], row['Unit Name'], \
            row['Lease Years'], row['Current Rent']
            if lines in ['01 Mar 1994', '24 Jun 1999', '30 Jan 2004', '08 Nov 2004', '26 Jul 2007', '21 Aug 2007']:
                print(reader, datetime.strptime(lines, "%d %b %Y").strftime("%d/%m/%Y"))
                
convert_csv_date()
