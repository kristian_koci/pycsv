# CSV scripts 

So, these are little scripts to accomplish the tasks specified on the document from BinkHQ


We have 4 functions, for each item of the test:

1) **current_rent**: This corresponds to the 1st item in the tech test, I've used try catch to look for the file, DictReader from csv module, and lambda to get the "interesting" row, it also has `numrows` as a parameter to stop the script at the 5th row. And we use **itemgetter** from **operator** module.

2) **filter_sum** This corresponds to the 2nd item in the tech test, this one uses a list comprehension to filter by column items equal to `25 years`

3) **dict_counter**: This corresponds to the 3rd item in the tech test, it uses **Counter** from collections module, to count the items on the dictionary, and print item that appears once, or more than once in the column

4) **convert_csv_date**: This is the 4th item in the tech test, it is a bit hardcoded, since I didn't have the time to refactor the code, and the 'date' thing was a bit tricky from my perspective.

## Dependencies

There's no particular requirement for this script, it was developed on *Python 3.7.3*.

To call each function separately, as usual, You can type on python's console:

    f = binkhq.convert_csv_date()
    f = binkhq.current_rent(filename="MobilePhoneMasts.csv")
    f = binkhq.dict_counter(filename="MobilePhoneMasts.csv")
    f = binkhq.filter_sum(filename="MobilePhoneMasts.csv")



We have to obviously import binkhq script first:

    import binkhq

## UPDATE:

Added a few basic tests:

On the app root directory, run:

    python test.py -v