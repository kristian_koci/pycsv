import unittest
from binkhq import current_rent, read_csv


class ParseCSVTest(unittest.TestCase):

    def setUp(self):
        self.data = 'MobilePhoneMasts.csv'

    def test_csv_read_csv_headers(self):
        self.assertEqual(
            read_csv(self.data)[0],
            ['Property Name', 'Property Address [1]', 'Property  Address [2]', 'Property Address [3]', \
            'Property Address [4]', 'Unit Name', 'Tenant Name', 'Lease Start Date', 'Lease End Date', \
            'Lease Years', 'Current Rent']
            )

    def test_csv_read_csv_tenant_name(self):
        self.assertEqual(read_csv(self.data)[1][6], 'Arqiva Services ltd')

    def test_csv_read_csv_lease_start_end_date(self):
        self.assertEqual(read_csv(self.data)[1][7], '01 Mar 1994')
        self.assertEqual(read_csv(self.data)[1][8], '28 Feb 2058')

    def current_rent_file_error(self):
        with self.assertRaises(FileNotFoundError): current_rent()

    def test_csv_read_max_rent(self):
        self.assertEqual(read_csv(self.data)[3][10], 28327.09) # It's going to fail



if __name__ == '__main__':
    unittest.main()